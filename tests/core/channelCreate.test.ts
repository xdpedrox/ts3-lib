import channelCreate from '../../src/teamspeak/core/channelCreate'

jest.mock('../../src/tsConnect', () => {
  return {
    channelCreate: (name: string, properties = {}) => {
      return new Promise(resolve => resolve({ name, properties }))
    }
  }
})

describe('channelCreate', () => {
  test('creates public channel', async () => {
    const channelName = 'Channel Name'

    try {
      const response = await channelCreate('namesss', {}, false)
      console.log(response)
    } catch (err) {
      console.log(err)
    }
  })

  test('creates private channel', async () => {
    const channelName = 'Channel Name'

    try {
      const response = await channelCreate('namesss', {}, true)
      console.log(response)
    } catch (err) {
      console.log(err)
    }
  })

})
