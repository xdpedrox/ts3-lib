import ts3 from "../../tsConnect"
import * as ts3Core from "../core/index"
import errorHandler from "../../responses/errorHandlerM"
import ts from "typescript"

/**
 * Changes the name of the Team
 * @param {String} teamObjectID - Member objectID from the Database.
 * @param {String} name - New name for the team.
 */
const teamChangeName = async (
  mainChannelId: string,
  name: string,
  serverGroupId: string | null = null
): Promise<void> => {
  try {
    const channel = await ts3.getChannelById(mainChannelId)

    const prefix = channel?.name.split("♦")[0]
    //Edit the claimed channel
    await ts3Core.channelEdit(
      mainChannelId,
      {
        channel_name: `${prefix} ♦ ${name}`,
      },
      true
    )

    if (serverGroupId !== null) {
      await ts3.serverGroupRename(serverGroupId, name)
    }
  } catch (err) {
    throw errorHandler(err, "Failed to change team name")
  }
}

export default teamChangeName
