import * as ts3Core from "../core/index"
import errorHandler from "../../responses/errorHandlerM"

import { serverGroupCreateInterface } from "../../types"

/**
 * Creates a ServerGroup for the team.
 * @param {String} uuid - TeamSpeak Unique ID
 */
const teamCreateServerGroup = async (
  name: string,
  uuids: Array<string>
): Promise<serverGroupCreateInterface> => {
  try {
    //Make a Copy of the template ServerGroup
    const group = await ts3Core.serverGroupCreate(name)

    // Add team members to serverGroup
    await Promise.all(
      uuids.map(async (uuid) => {
        return await ts3Core.serverGroupAddUser(uuid, group.sgid)
      })
    )

    return group
  } catch (err) {
    throw errorHandler(err, "Failed to create ServerGroup - teamCreateServerGroup")
  }
}

export default teamCreateServerGroup
