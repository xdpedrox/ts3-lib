import errorHandler from "../../responses/errorHandlerM"
import teamSetPermission from "./teamSetPermission"

interface ResetUser {
  uuid: string
  permission: number
}

const teamResetPermissions = (
  mainChannelId: string,
  serverGroupId: string,
  users: Array<ResetUser>
) => {
  try {
    const addMembersToGroupArray = users.map((user) => {
      return teamSetPermission(
        mainChannelId,
        // @ts-ignore
        user.uuid,
        user.permission,
        serverGroupId
      )
    })

    return Promise.all(addMembersToGroupArray)
  } catch (err) {
    throw errorHandler(err, "Failed to reassing permissions")
  }
}

export default teamResetPermissions
