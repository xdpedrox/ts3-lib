import * as ts3Core from "../core/index"
import { groupSettings, channelSettings } from "../../../../shared/config/ts3config"
import ts3 from "../../tsConnect"
import channelInfo from "../core/channelInfo"
import subChannelList from "../core/subChannelList"
import errorHandler from "../../responses/errorHandlerM"

const teamDestroy = async (cid: number, serverGroupId: number | null = null) => {
  let found = false
  const cidString = "" + cid

  try {
    const channels = await channelInfo()

    for (const channel of channels) {
      // console.log(channelIsSpacer(channel))
      if (found) {
        if (channel.channelFlagPermanent && channel.pid == "0") {
          // if (channelIsSpacer(channel)) {

          // Se o canal for um spacer e se o canal nao tiver sub canais. e se nao for o ultimo canal
          const hasNoSubChannels = (await subChannelList(channel.cid)).length == 0
          if (hasNoSubChannels && channel.cid != channelSettings.endChannel) {
            await ts3.channelDelete(channel.cid, true)
          } else {
            break
          }
        }
      }
      if (channel.cid == cidString) {
        found = true
      }
    }
    await ts3.channelDelete(cidString, true)
    if (serverGroupId) await ts3.serverGroupDel("" + serverGroupId, true)
    return { cid: cidString, serverGroupId }
  } catch (err) {
    errorHandler(err, "Failed to Destroy team")
  }
}

export default teamDestroy
