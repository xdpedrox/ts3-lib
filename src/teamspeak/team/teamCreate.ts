import * as ts3Core from "../core/index"
import ts3 from "../../tsConnect"
import teamSetPermission from "./teamSetPermission"
import errorHandler from "../../responses/errorHandlerM"

const teamCreate = async (name: string, password: string, uuids: string[], move: boolean) => {
  try {
    const channelIds = await ts3Core.channelCreatePack(`[cspacer]1 ♦ ${name}`, password)

    //Add user to the Channel and Give Permissions
    await teamSetPermission(channelIds.mainChannelId, uuids, 1)
    if (move) {
      const PromMove = uuids.map(async (uuid) => {
        await ts3Core.moveUserToChannel("" + channelIds.firstChannel, uuid).catch(() => {
          console.log("Couldn't move user")
        })
      })

      await Promise.all(PromMove)
    }

    return { ...channelIds, move: move }
  } catch (err) {
    // TODO no error if user wasn't moved
    throw errorHandler(err, "Failed to create Team -  teamCreate")
  }
}

export default teamCreate
