import * as ts3Core from "../core/index"
import { groupSettings } from "../../../../shared/config/ts3config"
import errorHandler from "../../responses/errorHandlerM"

/**
 * Adds user to team, can be run multiple times without failure
 * @param mainChannelId
 * @param uuid
 * @param permission
 * @param serverGroupId
 * @returns
 */
const teamSetPermission = async (
  mainChannelId: string,
  uuids: string[],
  permission: number,
  serverGroupId: number | null = null
): Promise<void> => {
  const uuidsProms = uuids.map(async (uuid) => {
    try {
      let channelGroupId
      // Removing Permissions
      if (permission == 5) {
        if (serverGroupId) {
          await ts3Core.serverGroupRemoveUser(uuid, "" + serverGroupId)
        }
        await ts3Core.clientSetChannelGroup(groupSettings.guestChannelGroup, mainChannelId, uuid)
        return
      } else {
        // Addings Permissions
        if (permission == 1) {
          channelGroupId = groupSettings.channelAdmin
        } else if (permission == 2) {
          channelGroupId = groupSettings.channelAdmin
        } else if (permission == 3) {
          channelGroupId = groupSettings.channelMod
        } else if (permission == 4) {
          channelGroupId = groupSettings.channelMember
        }

        // Add to serverGroup
        if (serverGroupId != null) {
          await ts3Core.serverGroupAddUser(uuid, "" + serverGroupId)
        }

        if (channelGroupId) {
          await ts3Core.clientSetChannelGroup(channelGroupId, mainChannelId, uuid)
        }
      }
    } catch (err) {
      throw errorHandler(err, "Error setting permissions - teamSetPermission")
    }
  })
  await Promise.all(uuidsProms)
}

export default teamSetPermission
