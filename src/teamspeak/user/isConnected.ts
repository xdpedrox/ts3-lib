import errorHandlerM from "../../responses/errorHandlerM"
import getConnectedClientsByIp from "../core/getConnectedClientsByIp"

/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const isConnected = async (ip: string) => {
  try {
    return await getConnectedClientsByIp(ip)
  } catch (err) {
    console.log(err)
    throw errorHandlerM(err, "Error adding user to group - serverGroupAddUser")
  }
}

export default isConnected
