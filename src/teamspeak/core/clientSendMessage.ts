import ts3 from "../../tsConnect"

import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Sends message to all connected clients that have that uuid
 * @param {String} ip
 */
const clientSendMessage = async (uuid: string, message: string): Promise<void> => {
  try {
    // Get all clients with that uuid
    const clients = await ts3.clientList({ clientUniqueIdentifier: uuid })

    // send message to all clients
    const messageArrays = clients.map((client) => {
      client.message(message)
    })

    await Promise.all(messageArrays)
  } catch (err) {
    throw errorHandlerM(err, "Error sending message to client - clientSendMessage")
  }
}

export default clientSendMessage
