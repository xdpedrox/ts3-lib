import channelInfo from "./channelInfo"
/**
 * Get Array of SubChannels
 *
 * @cid:        int    Channel ID
 */
const subChannelList = (cid: string) => {
  return channelInfo({ pid: cid })
}

export default subChannelList
