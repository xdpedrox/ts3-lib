import ts3 from "../../tsConnect"
import getCldbidFromUid from "./getCldbidFromUid"

import errorHandlerM from "../../responses/errorHandlerM"

const clientSetDescription = async (uuid: string, description: string) => {
  try {
    const cldbid = await getCldbidFromUid(uuid)

    return ts3.clientDbEdit(cldbid, {
      clientDescription: description,
    })
  } catch (err) {
    throw errorHandlerM(err, "Error setting description - clientSetDescription")
  }
}

export default clientSetDescription
