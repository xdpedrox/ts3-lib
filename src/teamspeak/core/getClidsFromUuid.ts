import ts3 from "../../tsConnect"
import { ClientGetUidFromClid } from "ts3-nodejs-library/src/types/ResponseTypes"

import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Get Clid From cluid
 *
 * @uid:       String    users.js Unique Identifier
 */
const getClidsFromUuid = async (uuid: string): Promise<Array<ClientGetUidFromClid>> => {
  try {
    return await ts3.execute("clientgetids", { cluid: uuid })
  } catch (err) {
    throw errorHandlerM(err, "Error getting userids - getClidsFromUuid")
  }
}

export default getClidsFromUuid
