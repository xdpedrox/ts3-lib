import ts3 from "../../tsConnect"
import channelInfo from "./channelInfo"
export interface ClientInterface {
  cid: string
  clid: string
  uuid: string
  clientNickname: string
  permission: null | number
}

export interface ChannelsInterface {
  cid: string
  channelName: string
  clients: ClientInterface[]
}

/**
 *
 * @param cid ChannelId
 * @returns
 */
const channelGetConnectedClients = async (cid: string) => {
  const subchannelList = await channelInfo({ pid: cid })

  let subChannels = subchannelList.map((channel) => {
    const channelss: ChannelsInterface = {
      cid: channel.cid,
      channelName: channel.channelName,
      clients: [],
    }
    return channelss
  })

  const subChannelIds = subChannels.map((c) => c.cid)

  // @ts-ignore
  const clientList = await ts3.clientList({ cid: subChannelIds })

  await clientList.map((rawClient: any) => {
    const client: ClientInterface = {
      cid: rawClient.cid,
      clid: rawClient.clid,
      uuid: rawClient.uniqueIdentifier,
      clientNickname: rawClient.nickname,
      permission: null,
    }

    const channelIndex = subChannels.findIndex((x) => x.cid == client.cid)
    if (channelIndex != -1) {
      // Fetch used by uuid
      subChannels[channelIndex].clients.push({ ...client })
    }
  })

  return subChannels
}

export default channelGetConnectedClients
