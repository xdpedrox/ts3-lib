import ts3 from "../../tsConnect"
import { channelSettings } from "../../../../shared/config/ts3config"
/**
 * Finds the last Spacer between 2 channels.
 * @returns
 */
const findLastChannelId = async (): Promise<string> => {
  const channels = await ts3.channelList()

  let lastId: string | null = null
  let gotStart = false
  let gotEnd = false

  await channels.forEach((channel) => {
    if (channel.cid == channelSettings.startChannel) {
      gotStart = true
    }

    // As start channel has  been found. And doesn't have sub channels
    if (!gotStart || channel.pid != "0") {
      return
    }

    if (gotEnd || channel.cid == channelSettings.endChannel) {
      gotEnd = true
      return
    }

    lastId = channel.cid
  })

  if (lastId == null) {
    throw new Error("Last channelId not found")
  }

  return lastId
}
export default findLastChannelId
