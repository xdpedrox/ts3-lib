import ts3 from "../../tsConnect"
import { ChannelEntry } from "ts3-nodejs-library/src/types/ResponseTypes"

import errorHandlerM from "../../responses/errorHandlerM"
import { ResponseTypes } from "../../types"
/**
 * Get dbid From cluid
 *
 * @uid:       String    users.js Unique Identifier
 */
const channelInfo = async (
  filter: Partial<ResponseTypes.ChannelEntry> = {}
): Promise<ChannelEntry[]> => {
  try {
    const channels = await ts3.channelList(filter)
    return channels.map((channel) => {
      // @ts-ignore
      return { ...channel.propcache }
    })
  } catch (err) {
    throw errorHandlerM(err, "Error getting ClientInfo - channelInfo")
  }
}

export default channelInfo
