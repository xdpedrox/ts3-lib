import { channelSettings } from "../../../../shared/config/ts3config"
import channelCreate from "./channelCreate"
import findLastChannelId from "./getLastMainChannelId"
import { TeamSpeakChannel } from "ts3-nodejs-library"
import errorHandlerM from "../../responses/errorHandlerM"
import { publicChannelProperties } from "../../../../shared/config/channels"

/**
 * Creates a group of channels - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} topic - Topic of the channel
 * @param {String} description - Description of the channel
 * @param {Number} spacerNumber - Number for the spacer.
 * @param {Number} lastCid - cid of the last channel of that area.
 */
const channelCreatePack = async (name: string, password: string) => {
  try {
    const lastId = await findLastChannelId()
    if (lastId == null) {
      throw "Errore fetching id"
    }

    //Create the mainChannel
    const mainChannel = await channelCreate(
      name,
      {
        channel_order: lastId,
      },
      true
    )
    //Create Sub ChannelsChannel

    const channels: TeamSpeakChannel[] = await Promise.all([
      channelCreate(channelSettings.subChannelName + " 1", {
        cpid: mainChannel.cid,
        channel_password: password,
      }),
      channelCreate(channelSettings.subChannelName + " 2", {
        cpid: mainChannel.cid,
        channel_password: password,
      }),
      channelCreate(channelSettings.subChannelName + " 3", {
        cpid: mainChannel.cid,
        channel_password: password,
      }),
      channelCreate(channelSettings.awayChannelName, {
        cpid: mainChannel.cid,
        channel_password: password,
      }),
    ])

    //Create Spacer Bar
    const spacerNumber = Math.floor(Math.random() * 10000000)
    const spacerBar = await channelCreate(
      `[*spacer${spacerNumber}]━`,
      {
        channel_order: mainChannel.cid,
      },
      true
    )

    //Create Spacer Bar

    //Create a object with all the Channels Ids that were just created
    return {
      firstChannel: channels?.at(0)?.cid || 0,
      mainChannelId: mainChannel.cid,
      spacerBarId: spacerBar.cid,
    }
  } catch (err) {
    throw errorHandlerM(err, "Error creating group of channels - channelCreatePack")
  }
}

export default channelCreatePack
