import ts3 from "../../tsConnect"
import { groupSettings } from "../../../../shared/config/ts3config"
import response from "../../responses/response"

import errorHandlerM from "../../responses/errorHandlerM"

import { serverGroupCreateInterface } from "../../types"

/**
 * Create Server Group to be used for the Server Icons
 */
const serverGroupCreate = (name: string): Promise<serverGroupCreateInterface> => {
  //Make a Copy of the template ServerGroup
  return ts3
    .serverGroupCopy(groupSettings.serverGroupTemplate, "0", 1, name)
    .then((group) => {
      if (group.sgid) return { sgid: group.sgid }
      else throw response("Error serverGroupCreate - failed to make group", null)
    })
    .catch((err) => {
      throw errorHandlerM(err, "Error creating server group - serverGroupCreate")
    })
}

export default serverGroupCreate
