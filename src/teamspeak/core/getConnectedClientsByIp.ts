import ts3 from "../../tsConnect"
import { ResponseTypes } from "../../types"

/**
 * Uses the IP to check if user is connected to the teamspeak server
 * @param {String} ip
 */
const getConnectedClientsByIp = async (ip: string) => {
  // Get client list with the filtered ip and type
  const filter: Partial<ResponseTypes.ClientEntry> = {
    connectionClientIp: ip,
    clientType: 0,
  }
  try {
    const clients = await ts3.clientList(filter)

    return clients.map((client) => {
      return {
        cid: client.cid,
        clid: client.clid,
        nickname: client.nickname,
        uniqueIdentifier: client.uniqueIdentifier,
      }
    })
  } catch (err) {
    console.log(err)
    throw err
    // new errorHandlerM(err, "Error getting user by IP - getUserByIP", true)
  }
}

export default getConnectedClientsByIp
