// /**
//  *  Removes all Channel Groups from everyone in the team
//  * @param {Number} mainChannelId - Main Channel/Spacer ID
//  */
//  const removeChannelGroups = (mainChannelId) => {

//   cid = parseInt(mainChannelId);

//   //Get the List of the SubChannels;
//   return subChannelList(cid)
//     .then(channels => {

//       //Works Like a ForEach Loop but it's async
//       let promiseArr1 = channels.map(channel => {

//         //Works Like a ForEach Loop but it's async
//         let promiseArr2 = config.ChannelGroupsAdmin.map(channelGroupId => {
//           // return the promise to array

//           //Get ChannelGroup object
//           return ts3.getChannelGroupByID(channelGroupId)
//             .then(channelGroup => {

//               //Get the List of clients that are part of that ChannelGroup
//               return channelGroup.clientList(channel.propcache.cid)
//                 .then(clients => {

//                   //Works Like a ForEach Loop but it's async
//                   let promiseArr3 = clients.map(client => {
//                     // return the promise to array

//                     return ts3.setClientChannelGroup(config.groupSettings.guestChannelGroup, client.cid, client.cldbid)
//                       .then(() => {
//                         return true
//                       })
//                   })

//                   //3 FOR EACH
//                   //Resolves and Checks if there was any problem with executiong returns results.
//                   return Promise.all(promiseArr3)
//                     .then(resultsArray3 => {

//                       return resultsArray3;
//                     })
//                     .catch(err => {
//                       throw error(33, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//                     })
//                   //

//                 }).catch(err => {
//                   if (err.id == 1281) {
//                     return success(200, "No Members in the group", null)
//                   } else {
//                     throw error(34, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//                   }
//                 })
//             })
//             .catch(err => {
//               throw error(35, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//             })
//         })

//         //2 FOR EACH
//         //Resolves and Checks if there was any problem with executiong returns results.
//         return Promise.all(promiseArr2)
//           .then(resultsArray2 => {

//             return resultsArray2;

//           })
//           .catch(err => {
//             throw error(36, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//           })
//         //

//       })

//       //3 FOR EACH
//       //Resolves and Checks if there was any problem with executiong returns results.
//       return Promise.all(promiseArr1)
//         .then(resultsArray1 => {
//           return success(200, "ChannelGroups have been reset", null)
//         })
//         .catch(err => {
//           throw error(37, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//         })
//       //

//     }).catch(err => {
//       throw error(38, "Error Channel.setSubChannelsPublic - SubChannellist", err)
//     })
// }

export {}
