import ts3 from "../../tsConnect"
import { ClientGetDbidFromUid } from "ts3-nodejs-library/src/types/ResponseTypes"

import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Get dbid From cluid
 *
 * @uid:       String    users.js Unique Identifier
 */
const getCldbidFromUid = async (uid: string) => {
  try {
    const data: Array<ClientGetDbidFromUid> = await ts3.execute("clientgetdbidfromuid", {
      cluid: uid,
    })
    return data[0].cldbid
  } catch (err) {
    throw errorHandlerM(err, "Error getting clids - getCldbidFromUid")
  }
}

export default getCldbidFromUid
