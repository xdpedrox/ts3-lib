import ts3 from "../../tsConnect"
import getCldbidFromUid from "./getCldbidFromUid"
import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Removes client from ServerGroup
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 * @param {Number} sgid - ServerGroup ID
 */
const serverGroupRemoveUser = async (uuid: string, sgid: string): Promise<void> => {
  try {
    //Get users.js DBID
    const cldbid = await getCldbidFromUid(uuid)
    //Remove Server Group
    await ts3.serverGroupDelClient(cldbid, sgid).catch((err) => {
      if (err.id != "2563") throw err
    })
  } catch (err) {
    throw errorHandlerM(err, "Error removing user from servergroup - serverGroupRemoveUser")
  }
}

export default serverGroupRemoveUser
