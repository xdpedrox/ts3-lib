import ts3 from "../../tsConnect"
import errorHandlerM from "../../responses/errorHandlerM"

const channelSetSubChannelIcon = async (cid: string, iconId?: number) => {
  try {
    const subChannels = await ts3.channelList({ pid: cid })
    for (const subChannel of subChannels) {
      if (iconId) {
        await subChannel.setPerm({
          permname: "i_icon_id",
          permvalue: iconId,
          permnegated: false,
          permskip: false,
        })
      } else {
        await subChannel.delPerm("i_icon_id")
      }
    }
  } catch (err) {
    throw errorHandlerM(err, "Error setting description - channelSetSubChannelIcon")
  }
}

export default channelSetSubChannelIcon
