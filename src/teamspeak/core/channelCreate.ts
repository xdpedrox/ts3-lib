import ts3 from "../../tsConnect"
import { privateChannelProperties, publicChannelProperties } from "../../../../shared/config/channels"
import errorHandlerM from "../../responses/errorHandlerM"
import { TeamSpeakChannel } from "ts3-nodejs-library"

/**
 *
 * @param name
 * @param channelproperties
 * @param isPrivate
 * @returns
 */
const channelCreate = async (
  name: string,
  channelproperties = {},
  isPrivate = false
): Promise<TeamSpeakChannel> => {
  try {
    // Set the channel Plublic or Private
    const properties = isPrivate ? privateChannelProperties : publicChannelProperties

    return await ts3.channelCreate(name, {
      channel_name: name,
      ...properties,
      ...channelproperties,
    })
  } catch (err) {
    throw errorHandlerM(err, "Error creating channel - channelCreate")
  }
}

export default channelCreate
