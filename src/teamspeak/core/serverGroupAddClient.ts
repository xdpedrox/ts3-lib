import ts3 from "../../tsConnect"
import getCldbidFromUid from "./getCldbidFromUid"
import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const serverGroupAddUser = async (uuid: string, sgid: string): Promise<void> => {
  try {
    const cldbid = await getCldbidFromUid(uuid)

    await ts3.serverGroupAddClient(cldbid, sgid).catch((err) => {
      if (err.id != "2561") {
        throw err
      }
    })
  } catch (err) {
    console.log(err)
    throw errorHandlerM(err, "Error adding user to group - serverGroupAddUser")
  }
}

export default serverGroupAddUser
