import ts3 from "../../tsConnect"
import getClidsFromUuid from "./getClidsFromUuid"

import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Moves client to the first channel of a team.
 * @param {Number} mainChannelId - CID of the Main Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 */
const moveUserToChannel = async (cid: string, uuid: string): Promise<void> => {
  try {
    const clids = await getClidsFromUuid(uuid)

    await ts3.clientMove(clids[0].clid, "" + cid)
  } catch (err) {
    throw errorHandlerM(err, "Error moving user - moveUserToChannel")
  }
}

export default moveUserToChannel
