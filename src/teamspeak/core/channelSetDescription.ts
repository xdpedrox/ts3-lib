import ts3 from "../../tsConnect"
import errorHandlerM from "../../responses/errorHandlerM"

const channelSetDescription = async (cid: string, description: string) => {
  try {
    return ts3.channelEdit(cid, {
      channelDescription: description,
    })
  } catch (err) {
    throw errorHandlerM(err, "Error setting description - channelSetDescription")
  }
}

export default channelSetDescription
