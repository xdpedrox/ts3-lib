import ts3 from "../../tsConnect"
import { privateChannelProperties, publicChannelProperties } from "../../../../shared/config/channels"
import { ChannelEdit } from "ts3-nodejs-library/src/types/PropertyTypes"

import errorHandlerM from "../../responses/errorHandlerM"
import { ResponseError } from "ts3-nodejs-library"

/**
 * Edit teamspeak channel
 * @param cid ChannelId
 * @param channelproperties Channel Properties
 * @param isPrivate Set the channel private or public
 * @returns
 */
const channelEdit = async (
  cid: string,
  channelproperties: ChannelEdit = {},
  isPrivate: boolean
): Promise<void> => {
  //Set the channel Plublic or Private
  const properties = isPrivate ? publicChannelProperties : privateChannelProperties

  await ts3
    .channelEdit(cid, channelproperties)
    .catch(async (err: ResponseError) => {
      // does the channel already have that name
      if (err.id == "771") {
        delete channelproperties.channel_name
        await ts3.channelEdit(cid, properties)
      }
      throw err
    })
    .catch((err) => {
      throw errorHandlerM(err, "Error editing channel - channelEdit")
    })
}

export default channelEdit
