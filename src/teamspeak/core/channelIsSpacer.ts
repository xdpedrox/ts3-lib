import { ChannelEntry } from "ts3-nodejs-library/src/types/ResponseTypes"

const channelIsSpacer = (channel: ChannelEntry) => {
  const regex = new RegExp("^\\[[c|s|l|*]{0,1}spacer[A-Za-z0-9]*]")

  if (regex.test(channel.channelName) && channel.channelFlagPermanent && channel.pid == "0")
    return true
  else return false
}

export default channelIsSpacer
