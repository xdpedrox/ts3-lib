import ts3 from "../../tsConnect"
import subChannelList from "./subChannelList"
import getCldbidFromUid from "./getCldbidFromUid"

import errorHandlerM from "../../responses/errorHandlerM"

/**
 * Gives a user ChannelGroup Permission on all the subchannels.
 * @param {Number} sgid - ServerGroup ID
 * @param {Number} mainChannelId - Main id of the channel / spacer
 * @param {String} uuid  - TeamSpeak Client Unique ID
 */
const channelGroupSet = async (
  cgid: string,
  mainChannelId: string,
  uuid: string
): Promise<void> => {
  try {
    const cldbid = await getCldbidFromUid(uuid)

    //Get the List of the SubChannels;
    const channels = await subChannelList(mainChannelId)

    //Set ChannelGroup to user to all subchannels
    const promiseArr = channels.map((channel) => {
      return ts3.setClientChannelGroup(cgid, channel.cid, cldbid)
    })

    //Resolves and Checks if there was any problem with executiong returns results.
    await Promise.all(promiseArr)
  } catch (err) {
    throw errorHandlerM(err, "Error setting channelGroup - channelGroupSet")
  }
}

export default channelGroupSet
