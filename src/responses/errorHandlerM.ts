import { ResponseError } from "ts3-nodejs-library"
import TeamSpeakError from "./TeamSpeakError"
import UnExpectedError from "./UnExpectedError"

const errorHandler = (err: ResponseError | TeamSpeakError | any, errorMessage: string) => {
  if (err instanceof ResponseError) return new TeamSpeakError(err, errorMessage)
  else if (err instanceof TeamSpeakError) return err
  return new UnExpectedError(err, errorMessage)
}

export default errorHandler
