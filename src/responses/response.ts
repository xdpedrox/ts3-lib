export interface ResponseInterface {
  msg?: string
  data?: object
  check?: boolean
}

const response = (msg = "Error", data: any = {}) => {
  if (data && data.check) {
    return data
  }

  return {
    msg: msg,
    data: data,
    check: true,
  }
}

export default response
