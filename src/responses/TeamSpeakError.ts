import BaseError, { errors, errorTypes } from "./BaseError"
import { ResponseError } from "ts3-nodejs-library"

class TeamSpeakError extends BaseError {
  responseError: ResponseError

  constructor(
    responseError: ResponseError,
    description = "Something went wrong on the teamspeak yo",
    isOperational = true
  ) {
    let type: errorTypes

    if (responseError.id == "771") {
      type = errors.CHANNEL_NAME_ALREADY_IN_USE
    } else if (responseError.id == "770") {
      type = errors.USER_IS_ALREADY_IN_CHANNEL
    } else if (responseError.id == "1282") {
      type = errors.SERVERGROUP_NAME_ALREADY_IN_USE
    } else if (responseError.id == "2561") {
      type = errors.USER_ALREADY_BELONGS_TO_SERVERGROUP
    } else if (responseError.id == "2564") {
      type = errors.USER_CANT_ADD_TO_DEFAULT_SERVERGROUP
    } else if (responseError.id == "2564") {
      type = errors.USER_CANT_ADD_TO_DEFAULT_SERVERGROUP
    } else {
      type = errors.UNEXPECTED_TEAMSPEAK_ERROR
    }

    super(type, description, isOperational)
    this.responseError = responseError
  }
}

export default TeamSpeakError
