import BaseError, { errors, errorTypes } from "./BaseError"
import { ResponseError } from "ts3-nodejs-library"

class UnExpectedError extends BaseError {
  error: any

  constructor(
    responseError: any,
    description = "Something went wrong on the teamspeak yo",
    isOperational = true
  ) {
    super(errors.UNEXPECTED_ERROR, description, isOperational)
    this.error = responseError
  }
}

export default UnExpectedError
