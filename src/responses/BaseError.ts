export const errors = {
  USER_IS_ALREADY_IN_CHANNEL: "USER_IS_ALREADY_IN_CHANNEL",
  USER_ALREADY_BELONGS_TO_SERVERGROUP: "USER_ALREADY_BELONGS_TO_SERVERGROUP",
  USER_CANT_ADD_TO_DEFAULT_SERVERGROUP: "USER_CANT_ADD_TO_DEFAULT_SERVERGROUP",

  CHANNEL_NAME_ALREADY_IN_USE: "CHANNEL_NAME_ALREADY_IN_USE",

  SERVERGROUP_NAME_ALREADY_IN_USE: "GROUP_NAME_ALREADY_IN_USE",

  UNEXPECTED_TEAMSPEAK_ERROR: "TEAMSPEAK_ERROR",
  UNEXPECTED_ERROR: "UNEXPECTED_ERROR",
} as const

export type errorTypes = (typeof errors)[keyof typeof errors]

class BaseError extends Error {
  name: errorTypes
  isOperational: boolean

  constructor(name: errorTypes, description: string, isOperational: boolean) {
    super(description)

    Object.setPrototypeOf(this, new.target.prototype)
    this.name = name
    this.isOperational = isOperational
    // Error.captureStackTrace(this)
  }
}

export default BaseError
