import { TeamSpeak } from "ts3-nodejs-library"

let ts3: TeamSpeak

if (process.env.NODE_ENV === "development") {
  // In development mode, use a global variable so that the value
  // is preserved across module reloads caused by HMR (Hot Module Replacement).
  let globalWithTs3 = global as typeof globalThis & {
    _ts3?: TeamSpeak
  }

  if (!globalWithTs3._ts3) {
    //Creates a new Connection to a TeamSpeak Server
    globalWithTs3._ts3 = new TeamSpeak({
      host: "eliteportuguesa.pt",
      queryport: 10011,
      serverport: 9987,
      username: "zoneg",
      password: "yzzIerzs",
      nickname: "Channel-Panel",
      autoConnect: true,
      keepAlive: true,
    })
  }
  ts3 = globalWithTs3._ts3
} else {
  ts3 = new TeamSpeak({
    host: "eliteportuguesa.pt",
    queryport: 10011,
    serverport: 9987,
    username: "zoneg",
    password: "yzzIerzs",
    nickname: "Channel-Panel",
    autoConnect: true,
    keepAlive: true,
  })
}

//Error event gets fired when an Error during connecting or an Error during Processing of an Event happens
ts3.on("error", (e) => {
  console.log(e)
  console.log("LIB -Error - LIB ", e.message)
})

//Close event gets fired when the Connection to the TeamSpeak Server has been closed
//the e variable is not always set
ts3.on("close", (e) => {
  console.log("LIB - Connection has been closed!", e)
})

ts3.on("error", (e) => {
  console.log(e)
})

export default ts3
