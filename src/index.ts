import ts3 from "./tsConnect"
import * as ts3Core from "./teamspeak/core/index"
import teamCreate from "./teamspeak/team/teamCreate"
import teamDestroy from "./teamspeak/team/teamDestroy"
import teamSetPermission from "./teamspeak/team/teamSetPermission"
import teamChangeName from "./teamspeak/team/teamChangeName"
import teamCreateServerGroup from "./teamspeak/team/teamCreateServerGroup"
import teamResetPermissions from "./teamspeak/team/teamResetPermissions"
import isConnected from "./teamspeak/user/isConnected"
export {
  ts3,
  ts3Core,
  teamCreate,
  teamDestroy,
  teamSetPermission,
  teamChangeName,
  teamCreateServerGroup,
  teamResetPermissions,
  isConnected,
}
