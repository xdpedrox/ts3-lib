export * as ResponseTypes from "ts3-nodejs-library/src/types/ResponseTypes"
export * as PropertyTypes from "ts3-nodejs-library/src/types/PropertyTypes"
export * as contextTypes from "ts3-nodejs-library/src/types/context"

export interface serverGroupCreateInterface {
  sgid: string
}
